package vn.cns.vinaidmerchant.Utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;

import java.util.Timer;
import java.util.TimerTask;

import vn.cns.vinaidmerchant.Model.BaseModel;
import vn.cns.vinaidmerchant.R;

/**
 * Created by Ho Dong Trieu on 01/14/2019
 */
public class FlowUtils extends BaseModel {
    private static FlowUtils sInstance;
    private ProgressDialog mProgressDialog;
    private Runnable mRunnable;

    public interface OnOKclickDialog {
        void onClickOk();
    }

    private OnOKclickDialog listener;
    public void setOnclickDialogListener(OnOKclickDialog listener) {
        this.listener = listener;
    }

    public static FlowUtils getInstance() {
        if(sInstance == null) {
            sInstance = new FlowUtils();
        }
        return sInstance;
    }

    public void showLoadingDialog(Context context) {
        if (!isLoadingDialogShowing()) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage(context.getString(R.string.loading_string));
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }
    }

    public void dismissLoadingDialog() {
        if (isLoadingDialogShowing()) {
            try {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace();
            }
        }
    }

    private boolean isLoadingDialogShowing() {
        return mProgressDialog != null && mProgressDialog.isShowing();
    }

    public void showAlert(Context context, String title, String message) {
        Timer timer = new Timer();
        AlertDialog.Builder bld = new AlertDialog.Builder(context);
        bld.setTitle(title);
        bld.setMessage(message);
        bld.setPositiveButton(context.getString(R.string.ok), (dialogInterface, i) -> {
            timer.cancel();
            listener.onClickOk();
        });
        AlertDialog alertDialog = bld.create();
        alertDialog.show();
        mRunnable = () -> {
            alertDialog.dismiss();
            listener.onClickOk();
        };
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnMainThread(mRunnable);
            }
        },3*1000);
    }

}
