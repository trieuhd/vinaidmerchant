package vn.cns.vinaidmerchant.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.google.gson.Gson;

import vn.cns.vinaidmerchant.Model.ConfigPOS;

/**
 * Created by Ho Dong Trieu on 12/04/2018
 */
public class PreferencesUtils {
    private static final String KEY_CONFIG_POS = "configPOS";
    private static final String KEY_SAM_ID ="samId";
    @SuppressLint("StaticFieldLeak")
    private static PreferencesUtils mInstance;
    private SharedPreferences mSharedPreferences;
    private Context mContext;

    private PreferencesUtils(Context context) {
        mContext = context;
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public static void initInstance(Context context) {
        if (mInstance == null) {
            mInstance = new PreferencesUtils(context);
        }
    }

    public static PreferencesUtils getInstance() {
        return mInstance;
    }

    public void saveConfigPOS (ConfigPOS configPOS) {
        Gson gson = new Gson();
        String jsonStringConfig = (configPOS != null) ? gson.toJson(configPOS) : null;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(KEY_CONFIG_POS,jsonStringConfig);
        editor.apply();
    }

    public ConfigPOS loadConfig() {
        String jsonString = mSharedPreferences.getString(KEY_CONFIG_POS,null);
        if(jsonString == null) {
            return null;
        }
        return new Gson().fromJson(jsonString,ConfigPOS.class);
    }

    public boolean hasConfig() {
        String jsonString = mSharedPreferences.getString(KEY_CONFIG_POS,null);
        return jsonString != null;
    }

    public void saveStateInput (String samId) {
        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(KEY_SAM_ID,samId);
        editor.apply();
    }

    public boolean checkSaveState() {
        String samId = mSharedPreferences.getString(KEY_SAM_ID,null);
        return samId != null;
    }

    public String getSateInput () {
        String stateInput = mSharedPreferences.getString(KEY_SAM_ID,null);
        if(stateInput == null) {
            return null;
        }
        return stateInput;
    }
}
