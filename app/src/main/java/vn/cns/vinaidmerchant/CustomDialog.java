package vn.cns.vinaidmerchant;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import vn.cns.vinaidmerchant.Adapter.ListViewModel;
import vn.cns.vinaidmerchant.Model.KiotVietModel;

/**
 * Created by Ho Dong Trieu on 10/25/2018
 */
public class CustomDialog extends DialogFragment {

    public static final String TAG = CustomDialog.class.getSimpleName();

    public interface OnclickButtonOK {
        void clickOK(ListViewModel listViewModel, int oldQuantity, boolean isTheSame, String name, int newQuantity);
    }
    private OnclickButtonOK listener;

    @BindView(R.id.productName_dialog)
    Spinner productName;
    @BindView(R.id.quantity_dialog)
    EditText quantity_dialog;
    @BindView(R.id.price_dialog)
    EditText price_dialog;
    @BindView(R.id.image_sp)
    ImageView image_sp;
    @BindView(R.id.slot_dialog)
    TextView slot_dialog;

    private ListViewModel listViewModel;
    private List<KiotVietModel> kiotVietModelList;
    private int indexSelection;
    private List<String> autocomplete;
    private String name;

    public static CustomDialog newInstance(ListViewModel listViewModel, List<KiotVietModel> kiotVietModelList) {
        CustomDialog customDialog = new CustomDialog();
        Bundle args = new Bundle();
        args.putSerializable("object",listViewModel);
        args.putSerializable("kiotviet", (Serializable) kiotVietModelList);
        customDialog.setArguments(args);
        return customDialog;
    }

    @NonNull
    @SuppressLint({"InflateParams", "SetTextI18n"})
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.layout_dialog_custom, null);
        ButterKnife.bind(this,view);
        assert getArguments() != null;
        autocomplete = new ArrayList<>();
        listViewModel = (ListViewModel) getArguments().getSerializable("object");
        name = Objects.requireNonNull(listViewModel).getProductCode();
        Log.d(Constants.Tag + " dialog",name);
        kiotVietModelList = (List<KiotVietModel>) getArguments().getSerializable("kiotviet");
        if (kiotVietModelList != null) {
            for (int i = 0 ; i < kiotVietModelList.size() ; i ++ ) {
                autocomplete.add(kiotVietModelList.get(i).getProductName());
            }
        }

        listener = (OnclickButtonOK) getActivity();
        slot_dialog.setText("Ngăn số: " + listViewModel.getIndex());
        builder.setCancelable(false);
        addTextAutocomplete(autocomplete);
        builder.setView(view)
                // Add action buttons
                .setPositiveButton("OK", (dialog, id) -> {
                    // sign in the user ...
                    int oldQuantity = listViewModel.getQuantity();
                    listViewModel.setProductName(kiotVietModelList.get(indexSelection).getFullName());
                    listViewModel.setProductCode(kiotVietModelList.get(indexSelection).getCode());
                    listViewModel.setPrice(kiotVietModelList.get(indexSelection).getBasePrice());
                    try {
                        if (Integer.valueOf(quantity_dialog.getText().toString()) > 6) {
                            try {
                                listViewModel.setQuantity(6);
                            } catch (Throwable e) {
                                listViewModel.setQuantity(0);
                            }
                        } else {
                            try {
                                listViewModel.setQuantity(Integer.valueOf(quantity_dialog.getText().toString()));
                            } catch (Throwable e) {
                                listViewModel.setQuantity(0);
                            }
                        }
                        if (name.equals(listViewModel.getProductCode())) {
                            listener.clickOK(listViewModel, oldQuantity, true, name, listViewModel.getQuantity());
                        } else {
                            listener.clickOK(listViewModel, oldQuantity, false, name, listViewModel.getQuantity());
                        }
                    } catch (NumberFormatException ignored) {

                    }
                })
                .setNegativeButton("Cancel", (dialog, id) -> CustomDialog.this.getDialog().cancel());
        return builder.create();
    }

    private void addTextAutocomplete(List<String> autocomplete) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()),android.R.layout.simple_dropdown_item_1line,autocomplete);
        productName.setAdapter(adapter);
        for (int i = 0 ; i < autocomplete.size() ; i++) {
            if (kiotVietModelList.get(i).getProductCode().equals(listViewModel.getProductCode())) {
                productName.setSelection(i);
                name = listViewModel.getProductCode();
                Glide.with(getActivity()).load(kiotVietModelList.get(i).getImages()).into(image_sp);
                indexSelection = i;
            }
        }
        productName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (indexSelection == i) {
                    quantity_dialog.setText(listViewModel.getQuantity() + "");
                } else {
                    quantity_dialog.setText(6 + "");
                    Glide.with(getActivity()).load(kiotVietModelList.get(i).getImages()).into(image_sp);
                }
                indexSelection = i;
                price_dialog.setText(kiotVietModelList.get(i).getBasePrice());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
