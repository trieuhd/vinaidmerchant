package vn.cns.vinaidmerchant;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Ho Dong Trieu on 12/04/2018
 */
public class SelectBranchsDialog extends DialogFragment {

    List<String> data;
    private ItemSelected listener;
    public static final String TAG = SelectBranchsDialog.class.getSimpleName();

    public static SelectBranchsDialog newInstance(List<String> data) {
        SelectBranchsDialog selectBranchsDialog = new SelectBranchsDialog();
        Bundle args = new Bundle();
        args.putStringArrayList("data", (ArrayList<String>) data);
        selectBranchsDialog.setArguments(args);
        return selectBranchsDialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        data = Objects.requireNonNull(getArguments()).getStringArrayList("data");
        listener = (ItemSelected) getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        builder.setTitle(R.string.branchsSelect).setItems(data.toArray(new CharSequence[0]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.selectedItem(i);
            }
        });

        return builder.create();
    }

    public interface ItemSelected {
        void selectedItem(int i);
    }
}
