package vn.cns.vinaidmerchant;

import android.app.Application;

import vn.cns.vinaidmerchant.Utils.PreferencesUtils;

/**
 * Created by Ho Dong Trieu on 02/18/2019
 */
public class VinaIdApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        PreferencesUtils.initInstance(getApplicationContext());
    }
}
