package vn.cns.vinaidmerchant.View;

/**
 * Created by Ho Dong Trieu on 02/18/2019
 */
public interface BaseView {
    void showFailedDialog(String errorMessage);
    void ShowToast(String message);
    void showLoadingDialog();
    void dismissLoadingDialog();
}
