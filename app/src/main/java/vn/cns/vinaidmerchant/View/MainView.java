package vn.cns.vinaidmerchant.View;

import java.util.List;

import vn.cns.vinaidmerchant.Adapter.ListViewModel;
import vn.cns.vinaidmerchant.Model.KiotVietModel;
import vn.cns.vinaidmerchant.Model.Products.Product;
import vn.cns.vinaidmerchant.Model.RequestAddItem;
import vn.cns.vinaidmerchant.Model.ResultSlotNumber.ResultSlotNumber;

/**
 * Created by Ho Dong Trieu on 02/19/2019
 */
public interface MainView extends BaseView {
    void updateProduct(Product product, List<KiotVietModel> kiotVietModels, int total_quantity_kiot);
    void updateSlotNumber(ResultSlotNumber resultSlotNumber, int total_quantity_mcp);
    void updateListViewModel(List<ListViewModel> listViewModels);
    void updateSpinnerValue(List<String> spinnerValue);

    void updateDataShow(int mcp,int kiot,int number_slot);
    void updateIndexAfterFind(int position);

    void updateProcessDataBeforeSend(String message,List<RequestAddItem> requestAddItems);

    void updateSendRequestNewSlot(int code);
}
