package vn.cns.vinaidmerchant.View;

import vn.cns.vinaidmerchant.Model.Branchs.BranchKiot;
import vn.cns.vinaidmerchant.Model.Token;

/**
 * Created by Ho Dong Trieu on 02/18/2019
 */
public interface LoginView extends BaseView {
    void updateToken(Token token);
    void updateBranch(BranchKiot branchKiot);
}
