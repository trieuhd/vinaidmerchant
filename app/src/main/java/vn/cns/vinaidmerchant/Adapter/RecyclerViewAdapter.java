package vn.cns.vinaidmerchant.Adapter;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import vn.cns.vinaidmerchant.Constants;
import vn.cns.vinaidmerchant.R;

/**
 * Created by Ho Dong Trieu on 03/06/2019
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewModel> implements Filterable {

    public interface OnDataChangedListenerRV{
        void onDataChangedListener(ListViewModel listViewModel);
        void filterResult(List<ListViewModel> mListViewModelFiltered);
        void SetChangeInventory(int i, int quantity);
    }

    private static final int LAYOUT_RES_ID = R.layout.layout_listview;
    private List<ListViewModel> mListViewModels;
    private List<ListViewModel> mListViewModelFilter;
    private OnDataChangedListenerRV mListener;

    public RecyclerViewAdapter(List<ListViewModel> listViewModels) {
        mListViewModels = listViewModels;
        mListViewModelFilter = listViewModels;
    }

    public void setListener(OnDataChangedListenerRV listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewModel onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());
        View view = mInflater.inflate(LAYOUT_RES_ID,viewGroup,false);
        return new ViewModel(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewModel viewModel, int i) {
        final ListViewModel listViewModel = mListViewModelFilter.get(i);
        if(listViewModel.getIndex() < 10) {
            viewModel.index_rv.setText("0" + listViewModel.getIndex());
        } else {
            viewModel.index_rv.setText(String.valueOf(listViewModel.getIndex()));
        }
//        viewModel.myCustomEditTextListener.updatePosition(viewModel.getAdapterPosition());
        viewModel.productName_rv.setText(listViewModel.getProductName());
        viewModel.price_rv.setText(listViewModel.getPrice());
        viewModel.quantity_rv.setText(String.valueOf(listViewModel.getQuantity()));
    }

    @Override
    public int getItemCount() {
        return mListViewModelFilter.size();
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if(charString.equals("Tất cả")) {
                    mListViewModelFilter = mListViewModels;
                } else {
                    List<ListViewModel> listViewModels = new ArrayList<>();
                    for(ListViewModel row: mListViewModels ) {
                        if(row.getProductCode().equals(charString)) {
                            listViewModels.add(row);
                        }
                    }

                    mListViewModelFilter = listViewModels;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mListViewModelFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mListViewModelFilter = (List<ListViewModel>) filterResults.values;
                mListener.filterResult(mListViewModelFilter);
                notifyDataSetChanged();
            }
        };
    }

    public class ViewModel extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.index_rv)
        TextView index_rv;
        @BindView(R.id.productName_rv)
        TextView productName_rv;
        @BindView(R.id.quantity_rv)
        EditText quantity_rv;
        @BindView(R.id.price_rv)
        TextView price_rv;

//        public MyCustomEditTextListener myCustomEditTextListener;

        public ViewModel(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            index_rv.setOnClickListener(this);
            productName_rv.setOnClickListener(this);
            price_rv.setOnClickListener(this);
            quantity_rv.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    int quantity = 0;
                    try {
                        quantity = Integer.valueOf(charSequence.toString());
                        if(quantity > 6) {
                            mListener.SetChangeInventory(mListViewModelFilter.get(getAdapterPosition()).getIndex() - 1,6);
                            quantity_rv.setText(String.valueOf(6));
                        } else {
                            mListener.SetChangeInventory(mListViewModelFilter.get(getAdapterPosition()).getIndex() - 1,Integer.valueOf(charSequence.toString()));
                        }
                    } catch (NumberFormatException ignored) {

                    }

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
//            this.myCustomEditTextListener = myCustomEditTextListener;
        }

        @Override
        public void onClick(View view) {
            if(! (view.getId() == R.id.quantity_rv)) {
                mListener.onDataChangedListener(mListViewModelFilter.get(getAdapterPosition()));
            }
        }
    }

//    private class MyCustomEditTextListener implements TextWatcher {
//        private int position;
//
//        public void updatePosition(int position) {
//            this.position = position;
//        }
//
//        @Override
//        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
//            // no op
//        }
//
//        @Override
//        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
//            int quantity = 0;
//            try {
//                quantity = Integer.valueOf(charSequence.toString());
//            } catch (NumberFormatException e) {
//                return;
//            }
//            if(quantity > 5) {
//                mListener.SetChangeInventory(position,5);
//            } else {
//                mListener.SetChangeInventory(position,Integer.valueOf(charSequence.toString()));
//            }
//        }
//
//        @Override
//        public void afterTextChanged(Editable editable) {
//            // no op
//        }
//    }
}
