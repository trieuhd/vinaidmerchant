package vn.cns.vinaidmerchant.Model;

import android.support.annotation.NonNull;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vn.cns.vinaidmerchant.Constants;
import vn.cns.vinaidmerchant.Service.API_Service;

/**
 * Created by Ho Dong Trieu on 01/14/2019
 */
public class TokenModel extends BaseModel {

    public interface OnTokenRetrievedListener {
        void onRetrieveTokenResponse(boolean success, Token token, String errorMsg);
    }

    private OnTokenRetrievedListener mListener;
    private Runnable mOnTokenLoadedCallback;
    private Callback<Token> mGetTokenResponseCallback;
    private boolean isLoadedSuccessfully;
    private Token mToken;
    private String mErrorMsg;

    public TokenModel () {
        mOnTokenLoadedCallback = () -> {
            if(mListener != null) {
                mListener.onRetrieveTokenResponse(isLoadedSuccessfully,mToken,mErrorMsg);
            }
        };

        mGetTokenResponseCallback = new Callback<Token>() {
            @Override
            public void onResponse(@NonNull Call<Token> call, @NonNull Response<Token> response) {
                String errorMsg = null;
                Token token = null;
                boolean success = response.isSuccessful();

                if(success) {
                    if(response.code() == Constants.HTTP_RESPONSE_CODE_SUCCESS) {
                        token = response.body();
                    } else {
                        errorMsg = response.message();
                    }
                } else {
                    errorMsg = response.message();
                }

                callOnTokenLoadedCallback(success,token,errorMsg);
            }

            @Override
            public void onFailure(@NonNull Call<Token> call, @NonNull Throwable t) {
                callOnTokenLoadedCallback(false,null,t.getMessage());
            }
        };
    }

    public void retrieveToken(final API_Service api, Map<String,String> fieldMap) {
        new Thread(() -> api.RETURN_ACCESSTOKEN_KIOTVIET(fieldMap).enqueue(mGetTokenResponseCallback)).start();
    }

    public Token getResult() {
        return mToken;
    }

    public void setTokenChangedListener(OnTokenRetrievedListener listener) {
        mListener = listener;
    }

    private void callOnTokenLoadedCallback(boolean success, Token token, String errorMsg) {
        isLoadedSuccessfully = success;
        mToken = token;
        mErrorMsg = errorMsg;
        runOnMainThread(mOnTokenLoadedCallback);
    }
}
