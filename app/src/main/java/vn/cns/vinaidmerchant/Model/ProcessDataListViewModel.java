package vn.cns.vinaidmerchant.Model;

import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import vn.cns.vinaidmerchant.Adapter.ListViewModel;
import vn.cns.vinaidmerchant.Constants;
import vn.cns.vinaidmerchant.Model.Products.Product;
import vn.cns.vinaidmerchant.Model.ResultSlotNumber.ResultSlotNumber;

/**
 * Created by Ho Dong Trieu on 03/05/2019
 */
public class ProcessDataListViewModel extends BaseModel {
    public interface onDataProcessChangedListener {
        void onDataProcessListener(List<ListViewModel> listViewModels, List<String> value);
    }

    private onDataProcessChangedListener mListener;
    private List<ListViewModel> mListViewModels;
    private Runnable mDataProcessCallback;
    private ResultSlotNumber mResultSlotNumber;
    private Product mProduct;
    private List<String> valueSpinner;

    public ProcessDataListViewModel(ResultSlotNumber resultSlotNumber, Product product) {
        mProduct = product;
        mResultSlotNumber = resultSlotNumber;
        mDataProcessCallback = () -> {
            if(mListener != null) {
                mListener.onDataProcessListener(mListViewModels,valueSpinner);
            }
        };
    }

    public void ProcessDataInput() {
        new Thread(() -> {
            List<ListViewModel> listViewModels = new ArrayList<>();
            List<String> stringList = new ArrayList<>();
            stringList.add("Tất cả");
            Log.d(Constants.Tag,"check product size again: " + mProduct.getData().size());
            for(int i = 0 ; i < mResultSlotNumber.getContent().size() ; i ++) {
                ListViewModel listViewModel = new ListViewModel();
                for(int j = 0 ; j < mProduct.getData().size() ; j ++) {
                    if(!stringList.contains(mProduct.getData().get(j).getFullName())) {
                        stringList.add(mProduct.getData().get(j).getFullName());
                    }
                    if(mResultSlotNumber.getContent().get(i).getProductCode().equals(mProduct.getData().get(j).getCode())) {
                        listViewModel.setPrice(String.valueOf(mProduct.getData().get(j).getBasePrice()));
                    } else {
                        listViewModel.setPrice(mResultSlotNumber.getContent().get(i).getPrice() + "");
                    }
                    listViewModel.setQuantity(mResultSlotNumber.getContent().get(i).getQuantity());
                    listViewModel.setProductName(mResultSlotNumber.getContent().get(i).getProductName());
                    listViewModel.setProductCode(mResultSlotNumber.getContent().get(i).getProductCode());
                    listViewModel.setIndex(mResultSlotNumber.getContent().get(i).getSlotNumber());
                }
                listViewModels.add(listViewModel);
            }
            setOnDataProcess(listViewModels,stringList);
        }).start();
    }

    public void setListener(onDataProcessChangedListener listener) {
        mListener = listener;
    }

    private void setOnDataProcess(List<ListViewModel> listViewModels,List<String> valueSpinner) {
        mListViewModels = listViewModels;
        this.valueSpinner = valueSpinner;
        runOnMainThread(mDataProcessCallback);
    }
}
