package vn.cns.vinaidmerchant.Model;

/**
 * Created by Ho Dong Trieu on 02/18/2019
 */
public class ConfigPOS {

    private String client_id;
    private String client_secret;
    private String retailer;
    private String MCP_host;
    private String MCP_token;
    private String terminalId;

    public String getMCP_host() {
        return MCP_host;
    }

    public void setMCP_host(String MCP_host) {
        this.MCP_host = MCP_host;
    }

    public String getMCP_token() {
        return MCP_token;
    }

    public void setMCP_token(String MCP_token) {
        this.MCP_token = MCP_token;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getRetailer() {
        return retailer;
    }

    public void setRetailer(String retailer) {
        this.retailer = retailer;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getClient_secret() {
        return client_secret;
    }

    public void setClient_secret(String client_secret) {
        this.client_secret = client_secret;
    }
}
