package vn.cns.vinaidmerchant.Model;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

import vn.cns.vinaidmerchant.Constants;
import vn.cns.vinaidmerchant.Model.Branchs.BranchKiot;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vn.cns.vinaidmerchant.Service.API_Service;

/**
 * Created by Ho Dong Trieu on 02/18/2019
 */

public class BranchModel extends BaseModel {
    public interface OnBranchRetrievedListener {
        void onRetrieveBranchResponse(boolean success, BranchKiot branchKiot, String errorMsg);
    }

    private OnBranchRetrievedListener mListener;
    private Runnable mOnBranchLoadedCallback;
    private Callback<BranchKiot> mGetBranchResponseCallback;
    private boolean isLoadedSuccessfully;
    private BranchKiot mBranchKiot;
    private String mErrorMsg;

    public BranchModel () {
        mOnBranchLoadedCallback = () -> {
            if(mListener != null) {
                mListener.onRetrieveBranchResponse(isLoadedSuccessfully,mBranchKiot,mErrorMsg);
            }
        };

        mGetBranchResponseCallback = new Callback<BranchKiot>() {
            @Override
            public void onResponse(@NotNull Call<BranchKiot> call, @NotNull Response<BranchKiot> response) {
                String errorMsg = null;
                BranchKiot branchKiot = null;
                boolean success = response.isSuccessful();

                if(success) {
                    if(response.code() == Constants.HTTP_RESPONSE_CODE_SUCCESS) {
                        branchKiot = response.body();
                    } else {
                        errorMsg = response.message();
                    }
                } else {
                    errorMsg = response.message();
                }

                callOnBranchLoadedCallback(success,branchKiot,errorMsg);
            }

            @Override
            public void onFailure(@NotNull Call<BranchKiot> call, @NotNull Throwable t) {
                callOnBranchLoadedCallback(false,null,t.getMessage());
            }
        };
    }

    public void retrieveBranch(final API_Service api, Map<String,String> headerMap) {
        new Thread(() -> api.RETURN_BRANCH_KIOTVIET(headerMap).enqueue(mGetBranchResponseCallback)).start();
    }

    public BranchKiot getBranchData () {
        return mBranchKiot;
    }

    public void setBranchModelDataChangedListener(OnBranchRetrievedListener listener) {
        mListener = listener;
    }

    private void callOnBranchLoadedCallback(boolean success, BranchKiot branchKiot, String errorMsg) {
        isLoadedSuccessfully = success;
        mBranchKiot = branchKiot;
        mErrorMsg = errorMsg;
        runOnMainThread(mOnBranchLoadedCallback);
    }
}
