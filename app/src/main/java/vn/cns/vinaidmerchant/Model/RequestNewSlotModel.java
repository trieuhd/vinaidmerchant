package vn.cns.vinaidmerchant.Model;

import android.widget.ListView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vn.cns.vinaidmerchant.Constants;
import vn.cns.vinaidmerchant.Service.API_Service;

/**
 * Created by Ho Dong Trieu on 03/28/2019
 */
public class RequestNewSlotModel extends BaseModel {
    public interface OnRequestNewSlotListener {
        void onRequestResponse(boolean success, int code, String errorMsg);
    }

    private OnRequestNewSlotListener mListener;

    private Runnable mOnRequestNewSlotLoadedCallback;
    private Callback<Void> mRequestNewSlotCallback;
    private boolean isLoadedSuccessfully;
    private int mCode;
    private String mErrorMsg;

    public RequestNewSlotModel () {
        mOnRequestNewSlotLoadedCallback = () -> {
            if(mListener != null) {
                mListener.onRequestResponse(isLoadedSuccessfully,mCode,mErrorMsg);
            }
        };

        mRequestNewSlotCallback = new Callback<Void>() {
            @Override
            public void onResponse(@NotNull Call<Void> call, @NotNull Response<Void> response) {
                String errorMsg = null;
                int code = 0;
                boolean success = response.isSuccessful();

                if(success) {
                    if(response.code() == Constants.HTTP_RESPONSE_CODE_SUCCESS) {
                        code = response.code();
                    } else {
                        errorMsg = response.message();
                    }
                } else {
                    errorMsg = response.message();
                }

                callOnRequestNewSlotLoadedCallback(success,code,errorMsg);
            }

            @Override
            public void onFailure(@NotNull Call<Void> call, @NotNull Throwable t) {
                callOnRequestNewSlotLoadedCallback(false,0,t.getMessage());
            }
        };
    }

    public void setmListener(OnRequestNewSlotListener listener) {
        mListener = listener;
    }

    public void SendRequestNewSlot(API_Service api_service, String token, List<RequestAddItem> requestAddItems) {
        new Thread(() -> api_service.POST_ADDNEWSLOT(token,requestAddItems).enqueue(mRequestNewSlotCallback)).start();
    }

    private void callOnRequestNewSlotLoadedCallback(boolean success, int code, String errorMsg) {
        isLoadedSuccessfully = success;
        mCode = code;
        mErrorMsg = errorMsg;
        runOnMainThread(mOnRequestNewSlotLoadedCallback);
    }
}
