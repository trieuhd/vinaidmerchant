package vn.cns.vinaidmerchant.Model;

import java.util.ArrayList;
import java.util.List;

import vn.cns.vinaidmerchant.Adapter.ListViewModel;
import vn.cns.vinaidmerchant.Utils.DateUtils;

/**
 * Created by Ho Dong Trieu on 03/27/2019
 */
public class ProcessDataBeforeUpdateModel extends BaseModel {
    public interface OnProcessDataBeforUpdateResponse {
        void onProcessDataUpdateResponse(boolean success, List<RequestAddItem> requestAddItems, String errorMsg);
    }

    private OnProcessDataBeforUpdateResponse mListener;
    private Runnable mOnProcessDataCallback;
    private boolean isProcessSuccessFully;
    private List<RequestAddItem> mRequestAddItem;
    private String mErrorMsg;

    public ProcessDataBeforeUpdateModel() {
        mOnProcessDataCallback = () -> {
            if(mListener != null) {
                mListener.onProcessDataUpdateResponse(isProcessSuccessFully,mRequestAddItem,mErrorMsg);
            }
        };
    }

    public void processData(List<ListViewModel> listViewModels, List<KiotVietModel> kiotVietModels, String terminalId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<RequestAddItem> requestAddItems = new ArrayList<>();
                for(int i = 0 ; i < kiotVietModels.size() ; i ++) {
                    int sum = 0;
                    for (int j = 0 ; j < listViewModels.size() ; j ++) {
                        RequestAddItem requestAddItem = new RequestAddItem();
                        if(listViewModels.get(j).getProductCode().equals(kiotVietModels.get(i).getProductCode())) {
                            sum += listViewModels.get(j).getQuantity();
                            requestAddItem.setPrice(kiotVietModels.get(i).getBasePrice());
                            requestAddItem.setProductCode(kiotVietModels.get(i).getProductCode());
                            requestAddItem.setProductName(kiotVietModels.get(i).getFullName());
                            requestAddItem.setQuantity(listViewModels.get(j).getQuantity());
                            requestAddItem.setSlotNumber(listViewModels.get(j).getIndex());
                            requestAddItem.setTerminalId(terminalId);
                            String date_Time = DateUtils.GetCurrentDateTime("yyyy/MM/dd HH:mm:ss");
                            requestAddItem.setUpdateDatetime(date_Time);
                            requestAddItem.setSyncDatetime(date_Time);
                            requestAddItems.add(requestAddItem);
                        }
                    }

                    if(sum != kiotVietModels.get(i).getQuantity()) {
                        callOnProcessCallback(false,null, kiotVietModels.get(i).getFullName() + " là " + kiotVietModels.get(i).getQuantity());
                        return;
                    }
                }
                callOnProcessCallback(true, requestAddItems, null);
            }
        }).start();
    }

    public void setmListener(OnProcessDataBeforUpdateResponse mListener) {
        this.mListener = mListener;
    }

    private void callOnProcessCallback(boolean success, List<RequestAddItem> requestAddItems, String errorMsg) {
        isProcessSuccessFully = success;
        mRequestAddItem = requestAddItems;
        mErrorMsg = errorMsg;
        runOnMainThread(mOnProcessDataCallback);
    }
}
