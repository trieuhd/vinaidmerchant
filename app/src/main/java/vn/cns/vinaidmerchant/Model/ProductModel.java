package vn.cns.vinaidmerchant.Model;

import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vn.cns.vinaidmerchant.Constants;
import vn.cns.vinaidmerchant.Model.Products.Product;
import vn.cns.vinaidmerchant.R;
import vn.cns.vinaidmerchant.Service.API_Service;

/**
 * Created by Ho Dong Trieu on 01/18/2019
 */
public class ProductModel extends BaseModel {

    public interface OnProductDataRetrievedListener {
        void onRetrieveAllProductResponse(boolean success, Product product, List<KiotVietModel> kiotVietModels,int total_quantity_kiot, String errorMsg);
    }

    private OnProductDataRetrievedListener mListener;
    private Runnable mOnProductLoadedCallback;
    private Callback<Product> mGetProductResponseCallback;
    private boolean isLoadedSuccessFully;
    private Product mProduct;
    private String mErrorMsg;
    private List<KiotVietModel> mKiotVietProduct;
    private int total_quantity_kiot;

    public ProductModel(int branchId) {
        mOnProductLoadedCallback = () -> {
            if(mListener != null) {
                mListener.onRetrieveAllProductResponse(isLoadedSuccessFully,mProduct,mKiotVietProduct,total_quantity_kiot,mErrorMsg);
            }
        };

        mGetProductResponseCallback = new Callback<Product>() {
            @Override
            public void onResponse(@NotNull Call<Product> call, @NotNull Response<Product> response) {
                String errorMsg = null;
                Product product = null;
                int total_quantity = 0;
                List<KiotVietModel> kiotVietModels = new ArrayList<>();
                boolean success = response.isSuccessful();

                if(success) {
                    if(response.code() == Constants.HTTP_RESPONSE_CODE_SUCCESS) {
                        product = response.body();
                        Product temp = product;

                        if(product != null) {
                            for (int i = 0; i < temp.getData().size(); i++) {
                                if(!temp.getData().get(i).getAllowsSale()
                                        && !temp.getData().get(i).getIsActive() && temp.getData().get(i).getInventories() == null ) {
                                    product.getData().remove(temp.getData().get(i));
                                    i--;
                                    Log.d(Constants.Tag,"remove object " + temp.getData().get(i).getFullName() + " up");
                                } else {
                                    int temp_int = product.getData().get(i).getInventories().size();
                                    for(int k = 0 ; k < temp_int ; k++) {
                                        if(product.getData().get(i).getInventories().get(k).getBranchId() == branchId) {
                                            if(product.getData().get(i).getInventories().get(k).getOnHand() > 0) {
                                                KiotVietModel kiotVietModel = new KiotVietModel();
                                                kiotVietModel.setProductCode(product.getData().get(i).getInventories().get(k).getProductCode());
                                                kiotVietModel.setProductName(product.getData().get(i).getInventories().get(k).getProductName());
                                                kiotVietModel.setCode(product.getData().get(i).getCode());
                                                kiotVietModel.setBasePrice(product.getData().get(i).getBasePrice().toString());
                                                kiotVietModel.setFullName(product.getData().get(i).getFullName());
                                                kiotVietModel.setQuantity(product.getData().get(i).getInventories().get(k).getOnHand());
                                                if(product.getData().get(i).getImages() != null) {
                                                    if (product.getData().get(i).getImages().size() > 0) {
                                                        kiotVietModel.setImages(product.getData().get(i).getImages().get(0));
                                                    }
                                                }
                                                kiotVietModels.add(kiotVietModel);
                                                total_quantity += kiotVietModel.getQuantity();
                                            } else {
                                                product.getData().remove(temp.getData().get(i));
                                                i--;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    } else {
                        errorMsg = response.message();
                    }
                } else {
                    errorMsg = response.message();
                }

                callOnProductLoadedCallback(success,product,kiotVietModels,total_quantity,errorMsg);
            }

            @Override
            public void onFailure(@NotNull Call<Product> call, @NotNull Throwable t) {
                callOnProductLoadedCallback(false,null,null,0,t.getMessage());
            }
        };
    }

    public void retrieveProduct(final API_Service api, Map<String,String> headerMap, int branchId) {
        new Thread(() -> api.RETURN_PRODUCT_KIOT(headerMap,branchId).enqueue(mGetProductResponseCallback)).start();
    }

    public Product getResult () {
        return mProduct;
    }

    public void setProductModelDataChangedListener(OnProductDataRetrievedListener listener) {
        mListener = listener;
    }

    private void callOnProductLoadedCallback(boolean success, Product product, List<KiotVietModel> kiotVietModels , int total_quantity , String errorMsg) {
        isLoadedSuccessFully = success;
        mProduct = product;
        mKiotVietProduct = kiotVietModels;
        total_quantity_kiot = total_quantity;
        mErrorMsg = errorMsg;
        runOnMainThread(mOnProductLoadedCallback);
    }
}
