package vn.cns.vinaidmerchant.Model;

import android.os.Handler;

import static android.os.Looper.getMainLooper;

/**
 * Created by Ho Dong Trieu on 01/14/2019
 */
public class BaseModel {
    public void runOnMainThread(Runnable runnable) {
        Handler handler = new Handler(getMainLooper());
        handler.post(runnable);
    }
}
