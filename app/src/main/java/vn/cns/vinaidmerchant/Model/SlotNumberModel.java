package vn.cns.vinaidmerchant.Model;

import android.util.Log;

import org.jetbrains.annotations.NotNull;

import vn.cns.vinaidmerchant.Constants;
import vn.cns.vinaidmerchant.Model.ResultSlotNumber.ResultSlotNumber;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vn.cns.vinaidmerchant.Service.API_Service;
import vn.cns.vinaidmerchant.Utils.PreferencesUtils;

/**
 * Created by Ho Dong Trieu on 02/19/2019
 */
public class SlotNumberModel extends BaseModel {
    public interface OnSlotNumberDataRetrievedListener {
        void onRetrieveSlotNumberDataResponse(boolean success, ResultSlotNumber resultSlotNumber,int total_quantity_mcp, String errorMsg);
    }

    private OnSlotNumberDataRetrievedListener mListener;
    private Runnable mOnSlotNumberDataLoadedCallback;
    private Callback<ResultSlotNumber> mGetSlotNumberDataResponseCallback;
    private boolean isLoadedSuccessFully;
    private ResultSlotNumber mResultSlotNumber;
    private String mErrorMsg;
    private int total_quantity_mcp;

    public SlotNumberModel() {
        mOnSlotNumberDataLoadedCallback = () -> {
            if(mListener != null) {
                mListener.onRetrieveSlotNumberDataResponse(isLoadedSuccessFully,mResultSlotNumber,total_quantity_mcp,mErrorMsg);
            }
        };

        mGetSlotNumberDataResponseCallback = new Callback<ResultSlotNumber>() {
            @Override
            public void onResponse(@NotNull Call<ResultSlotNumber> call, @NotNull Response<ResultSlotNumber> response) {
                String errorMsg = null;
                ResultSlotNumber resultSlotNumber = null;
                int total_quantity = 0;
                boolean success = response.isSuccessful();
                Log.d(Constants.Tag,"code slotnumber: " + response.code());
                if(success) {
                    switch (response.code()) {
                        case Constants.HTTP_RESPONSE_CODE_SUCCESS:
                            Log.d(Constants.Tag,"go here success");
                            resultSlotNumber = response.body();
                            if (resultSlotNumber != null) {
                                for(int i = 0 ; i < resultSlotNumber.getContent().size() ; i ++) {
                                    total_quantity += resultSlotNumber.getContent().get(i).getQuantity();
                                }
                            }
                            break;
                        case 401:
                            errorMsg = "UnAuthorization";
                            break;
                        case 500:
                            errorMsg = "Internal Server Error";
                            break;
                            default:
                                errorMsg = response.message();
                                break;
                    }
                } else {
                    errorMsg = response.message();
                }

                callOnSlotNumberDataLoadedCallback(success,resultSlotNumber,total_quantity,errorMsg);
            }

            @Override
            public void onFailure(@NotNull Call<ResultSlotNumber> call, @NotNull Throwable t) {
                callOnSlotNumberDataLoadedCallback(false,null,0,t.getMessage());
            }
        };
    }

    public void retrieveSlotNumberData(final API_Service api, String token, String terminalId) {
        new Thread(() -> api.GET_SLOT_NUMBERS(token,terminalId,100,0).enqueue(mGetSlotNumberDataResponseCallback)).start();
    }

    public ResultSlotNumber getResultData() {
        return mResultSlotNumber;
    }

    public void setSlotNumberModelDataChangedListener(OnSlotNumberDataRetrievedListener listener) {
        mListener = listener;
    }

    private void callOnSlotNumberDataLoadedCallback(boolean success, ResultSlotNumber resultSlotNumber,int total_quantity, String errorMsg) {
        isLoadedSuccessFully = success;
        mResultSlotNumber = resultSlotNumber;
        total_quantity_mcp = total_quantity;
        mErrorMsg = errorMsg;
        runOnMainThread(mOnSlotNumberDataLoadedCallback);
    }
}
