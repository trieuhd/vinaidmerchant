package vn.cns.vinaidmerchant;

/**
 * Created by Ho Dong Trieu on 10/15/2018
 */
public interface Constants {
    String Tag = "HDT";
    String PREFEREENCES_NAME = "MY_TOKEN";
    int HTTP_RESPONSE_CODE_SUCCESS = 200;

    String API_RESPONSE_UTC_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
}
