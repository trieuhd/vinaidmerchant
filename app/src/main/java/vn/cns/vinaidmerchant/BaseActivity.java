package vn.cns.vinaidmerchant;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;
import vn.cns.vinaidmerchant.Utils.FlowUtils;
import vn.cns.vinaidmerchant.View.BaseView;

/**
 * Created by Ho Dong Trieu on 02/18/2019
 */
@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity implements BaseView {

    @Override
    public void showFailedDialog(String errorMessage) {
        if(errorMessage == null) {
            errorMessage = this.getString(R.string.unexpected_error_message);
        }
        FlowUtils.getInstance().showAlert(this,this.getString(R.string.error),errorMessage);
    }

    @Override
    public void ShowToast(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setGravity(Gravity.TOP, 0, 40);
        toast.show();
    }

    @Override
    public void showLoadingDialog() {
        FlowUtils.getInstance().showLoadingDialog(this);
    }

    @Override
    public void dismissLoadingDialog() {
        FlowUtils.getInstance().dismissLoadingDialog();
    }

    protected void showLog(String msg) {
        Log.d(Constants.Tag,msg);
    }
}
