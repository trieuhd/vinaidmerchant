package vn.cns.vinaidmerchant.Presenter;

import vn.cns.vinaidmerchant.Model.Branchs.BranchKiot;
import vn.cns.vinaidmerchant.Model.ConfigPOS;
import vn.cns.vinaidmerchant.Service.API_Service;

/**
 * Created by Ho Dong Trieu on 02/18/2019
 */
public interface LoginPresenter {
    void retrieveToken();
    void retrieveBranch(String token);
    BranchKiot getDataBranch();
    void setConfig(ConfigPOS config);
}
