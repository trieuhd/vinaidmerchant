package vn.cns.vinaidmerchant.Presenter;

import java.util.List;
import java.util.Map;

import vn.cns.vinaidmerchant.Adapter.ListViewModel;
import vn.cns.vinaidmerchant.Model.KiotVietModel;
import vn.cns.vinaidmerchant.Model.Products.Product;
import vn.cns.vinaidmerchant.Model.RequestAddItem;
import vn.cns.vinaidmerchant.Model.ResultSlotNumber.ResultSlotNumber;
import vn.cns.vinaidmerchant.Service.API_Service;

/**
 * Created by Ho Dong Trieu on 02/19/2019
 */
public interface MainPresenter {
    void retrieveProduct(Map<String,String> headerMap, int branchId);
    void retrieveSlotNumber(API_Service api, String token, String terminalId);
    void processDataAfterTake(ResultSlotNumber resultSlotNumber, Product product);
    void CalculateDataFilter(List<ListViewModel> listViewModels, List<KiotVietModel> kiotVietModels, boolean isAll);

    void FindIndex(String code , List<KiotVietModel> kiotVietModels);

    void processDatabeforeSend(List<ListViewModel> listViewModels, List<KiotVietModel> kiotVietModels, String terminalId);

    void retrieveRequestNewSlot(API_Service api_service, String token, List<RequestAddItem> requestAddItems);
}
