package vn.cns.vinaidmerchant.Presenter;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import vn.cns.vinaidmerchant.Constants;
import vn.cns.vinaidmerchant.Model.BranchModel;
import vn.cns.vinaidmerchant.Model.Branchs.BranchKiot;
import vn.cns.vinaidmerchant.Model.ConfigPOS;
import vn.cns.vinaidmerchant.Model.Token;
import vn.cns.vinaidmerchant.Model.TokenModel;
import vn.cns.vinaidmerchant.Service.API_Service;
import vn.cns.vinaidmerchant.Service.RetrofitService;
import vn.cns.vinaidmerchant.Service.URLUtils;
import vn.cns.vinaidmerchant.View.LoginView;

/**
 * Created by Ho Dong Trieu on 02/18/2019
 */
public class LoginPresenterImp implements LoginPresenter, TokenModel.OnTokenRetrievedListener, BranchModel.OnBranchRetrievedListener {

    private LoginView mView;
    private TokenModel mTokenModel;
    private BranchModel mBranchModel;
    private ConfigPOS mConfigPOS;

    public LoginPresenterImp (LoginView view) {
        mView = view;

        mTokenModel = new TokenModel();
        mTokenModel.setTokenChangedListener(this);
        mBranchModel = new BranchModel();
        mBranchModel.setBranchModelDataChangedListener(this);
    }

    @Override
    public void setConfig(ConfigPOS config) {
        mConfigPOS = config;
    }

    @Override
    public void retrieveToken() {
        if(mView == null || mTokenModel == null) {
            Log.d(Constants.Tag," null retrieve token");
            return;
        }

        Log.d(Constants.Tag,"retrieve token");

        URLUtils urlUtils = new URLUtils("https://id.kiotviet.vn/");
        API_Service api_service = urlUtils.getService();
        Map<String, String> client_map = new HashMap<>();
        client_map.put("client_id",mConfigPOS.getClient_id());
        client_map.put("client_secret",mConfigPOS.getClient_secret());
        client_map.put("grant_type","client_credentials");
        client_map.put("scopes","PublicApi.Access");
        mTokenModel.retrieveToken(api_service,client_map);
    }

    @Override
    public void retrieveBranch(String token) {
        if(mView == null || mBranchModel == null) {
            return;
        }

        URLUtils urlUtils = new URLUtils("https://public.kiotapi.com/");
        API_Service api_service = urlUtils.getService();
        Map<String,String> headerMap = new HashMap<>();
        headerMap.put("Retailer",mConfigPOS.getRetailer());
        headerMap.put("Authorization","Bearer " + token);
        mBranchModel.retrieveBranch(api_service,headerMap);
    }

    @Override
    public BranchKiot getDataBranch() {
        return mBranchModel.getBranchData();
    }

    @Override
    public void onRetrieveTokenResponse(boolean success, Token token, String errorMsg) {
        if(mView == null) {
            return;
        }

        if(success) {
            mView.updateToken(token);
        } else {
            mView.updateToken(null);
            mView.showFailedDialog(errorMsg);
        }
    }

    @Override
    public void onRetrieveBranchResponse(boolean success, BranchKiot branchKiot, String errorMsg) {
        if(mView == null) {
            return;
        }

        if(success) {
            mView.updateBranch(branchKiot);
        } else {
            mView.showFailedDialog(errorMsg);
        }
    }
}
