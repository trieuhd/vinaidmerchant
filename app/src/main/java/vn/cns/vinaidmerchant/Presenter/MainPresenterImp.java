package vn.cns.vinaidmerchant.Presenter;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Handler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import vn.cns.vinaidmerchant.Adapter.ListViewModel;
import vn.cns.vinaidmerchant.Model.KiotVietModel;
import vn.cns.vinaidmerchant.Model.ProcessDataBeforeUpdateModel;
import vn.cns.vinaidmerchant.Model.ProcessDataListViewModel;
import vn.cns.vinaidmerchant.Model.ProductModel;
import vn.cns.vinaidmerchant.Model.Products.Product;
import vn.cns.vinaidmerchant.Model.RequestAddItem;
import vn.cns.vinaidmerchant.Model.RequestNewSlotModel;
import vn.cns.vinaidmerchant.Model.ResultSlotNumber.ResultSlotNumber;
import vn.cns.vinaidmerchant.Model.SlotNumberModel;
import vn.cns.vinaidmerchant.Service.API_Service;
import vn.cns.vinaidmerchant.Service.URLUtils;
import vn.cns.vinaidmerchant.View.MainView;

import static android.os.Looper.getMainLooper;

/**
 * Created by Ho Dong Trieu on 02/19/2019
 */
public class MainPresenterImp implements MainPresenter, SlotNumberModel.OnSlotNumberDataRetrievedListener, ProductModel.OnProductDataRetrievedListener, ProcessDataListViewModel.onDataProcessChangedListener, ProcessDataBeforeUpdateModel.OnProcessDataBeforUpdateResponse, RequestNewSlotModel.OnRequestNewSlotListener {

    private MainView mView;
    private SlotNumberModel mSlotNumberModel;
    private ProductModel mProductModel;
    private ProcessDataBeforeUpdateModel mProcessDataBeforeUpdateModel;
    private RequestNewSlotModel mRequestNewSlotModel;

    public MainPresenterImp (MainView view, int branchId) {
        mView = view;

        mSlotNumberModel = new SlotNumberModel();
        mSlotNumberModel.setSlotNumberModelDataChangedListener(this);

        mProductModel = new ProductModel(branchId);
        mProductModel.setProductModelDataChangedListener(this);

        mProcessDataBeforeUpdateModel = new ProcessDataBeforeUpdateModel();
        mProcessDataBeforeUpdateModel.setmListener(this);

        mRequestNewSlotModel = new RequestNewSlotModel();
        mRequestNewSlotModel.setmListener(this);
    }

    @Override
    public void onRetrieveSlotNumberDataResponse(boolean success, ResultSlotNumber resultSlotNumber,int total_quantity_mcp, String errorMsg) {
        if(mView == null) {
            return;
        }

        mView.dismissLoadingDialog();
        if(success) {
            mView.updateSlotNumber(resultSlotNumber,total_quantity_mcp);
        } else {
            mView.showFailedDialog(errorMsg);
        }
    }

    @Override
    public void onRetrieveAllProductResponse(boolean success, Product product, List<KiotVietModel> kiotVietModels,int total_quantity_kiot, String errorMsg) {
        if(mView == null) {
            return;
        }

        if(success) {
            mView.updateProduct(product,kiotVietModels,total_quantity_kiot);
        } else {
            mView.dismissLoadingDialog();
            mView.showFailedDialog(errorMsg);
        }
    }

    @Override
    public void retrieveProduct(Map<String,String> headerMap, int branchId) {
        if(mView == null || mProductModel ==null) {
            return;
        }

        mView.showLoadingDialog();
        URLUtils urlUtils = new URLUtils("https://public.kiotapi.com/");
        API_Service api = urlUtils.getService();
        mProductModel.retrieveProduct(api,headerMap,branchId);
    }

    @Override
    public void retrieveSlotNumber(API_Service api,String token, String terminalId) {
        if(mView == null || mSlotNumberModel == null) {
            return;
        }

        mSlotNumberModel.retrieveSlotNumberData(api,token,terminalId);
    }

    @Override
    public void processDataAfterTake(ResultSlotNumber resultSlotNumber, Product product) {
        ProcessDataListViewModel mProcessDataListViewModel = new ProcessDataListViewModel(resultSlotNumber, product);
        mProcessDataListViewModel.setListener(this);
        mProcessDataListViewModel.ProcessDataInput();
    }

    @Override
    public void onDataProcessListener(List<ListViewModel> listViewModels,List<String> value) {
        if(mView == null) {
            return;
        }

        mView.updateListViewModel(listViewModels);
        mView.updateSpinnerValue(value);
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void CalculateDataFilter(List<ListViewModel> listViewModels, List<KiotVietModel> kiotVietModels,boolean isAll) {
        final int[] mcp = {0};
        final int[] kiot = {0};
        int number_slot = listViewModels.size();
        if(number_slot == 0) {
            mView.updateDataShow(0,0,0);
            return;
        }
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                for(int j = 0 ; j < listViewModels.size() ; j ++) {
                    mcp[0] += listViewModels.get(j).getQuantity();
                }

                if(!isAll) {
                    for (int i = 0; i < kiotVietModels.size(); i++) {
                        if (listViewModels.get(0).getProductCode().equals(kiotVietModels.get(i).getProductCode())) {
                            kiot[0] += kiotVietModels.get(i).getQuantity();
                        }
                    }
                } else {
                    if (kiot[0] == 0) {
                        for (int i = 0; i < kiotVietModels.size(); i++) {
                            kiot[0] += kiotVietModels.get(i).getQuantity();
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if(mView ==null) {
                    return;
                }

                mView.updateDataShow(mcp[0],kiot[0],number_slot);
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void FindIndex(String code , List<KiotVietModel> kiotVietModels) {
        new AsyncTask<Integer, Void, Integer>() {
            @Override
            protected Integer doInBackground(Integer... voids) {
                for(int i = 0 ; i < kiotVietModels.size() ; i ++) {
                    if(code.equals(kiotVietModels.get(i).getProductCode())) {
                        return i;
                    }
                }
                return -1;
            }

            @Override
            protected void onPostExecute(Integer position) {
                mView.updateIndexAfterFind(position);
            }
        }.execute();
    }

    @Override
    public void processDatabeforeSend(List<ListViewModel> listViewModels, List<KiotVietModel> kiotVietModels, String terminalId) {
        if(mView == null || mProcessDataBeforeUpdateModel == null) {
            return;
        }

        mView.showLoadingDialog();
        mProcessDataBeforeUpdateModel.processData(listViewModels,kiotVietModels,terminalId);
    }

    @Override
    public void onProcessDataUpdateResponse(boolean success, List<RequestAddItem> requestAddItems, String errorMsg) {
        if(success) {
//            mView.dismissLoadingDialog();
            mView.updateProcessDataBeforeSend(null,requestAddItems);
        } else {
            mView.dismissLoadingDialog();
            mView.updateProcessDataBeforeSend(errorMsg,requestAddItems);
        }
    }

    @Override
    public void retrieveRequestNewSlot(API_Service api_service, String token, List<RequestAddItem> requestAddItems) {
        if(mView == null || mRequestNewSlotModel == null) {
            return;
        }

        mRequestNewSlotModel.SendRequestNewSlot(api_service,token,requestAddItems);
    }

    @Override
    public void onRequestResponse(boolean success, int code, String errorMsg) {
        mView.dismissLoadingDialog();
        mView.updateSendRequestNewSlot(code);
    }
}
