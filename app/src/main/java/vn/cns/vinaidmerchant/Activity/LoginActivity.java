package vn.cns.vinaidmerchant.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vn.cns.vinaidmerchant.BaseActivity;
import vn.cns.vinaidmerchant.Constants;
import vn.cns.vinaidmerchant.Model.Branchs.BranchKiot;
import vn.cns.vinaidmerchant.Model.ConfigPOS;
import vn.cns.vinaidmerchant.Model.ResultSlotNumber.Content;
import vn.cns.vinaidmerchant.Model.Token;
import vn.cns.vinaidmerchant.Presenter.LoginPresenter;
import vn.cns.vinaidmerchant.Presenter.LoginPresenterImp;
import vn.cns.vinaidmerchant.R;
import vn.cns.vinaidmerchant.SelectBranchsDialog;
import vn.cns.vinaidmerchant.Utils.KeyGenerate;
import vn.cns.vinaidmerchant.Utils.PreferencesUtils;
import vn.cns.vinaidmerchant.View.LoginView;

public class LoginActivity extends BaseActivity implements LoginView, SelectBranchsDialog.ItemSelected {

    @BindView(R.id.login_progress)
    View mProgressView;
    @BindView(R.id.linear_login)
    View mLoginFormView;
    @BindView(R.id.samId)
    EditText mSamId;
    @BindView(R.id.checkbox)
    CheckBox mCheckbox;

    private LoginPresenter mPresenter;
    private String mStringData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        String samId = PreferencesUtils.getInstance().getSateInput();
        if(samId != null) {
            mCheckbox.setChecked(true);
            mSamId.setText(samId);
        }

        mPresenter = new LoginPresenterImp(this);
    }

    @OnClick(R.id.button_login)
    public void Login() {
        attemptLogin();
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        // Reset errors.
        mSamId.setError(null);

        // Store values at the time of the login attempt.
        String samId = mSamId.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(samId)) {
            mSamId.setError(getString(R.string.error_field_required));
            focusView = mSamId;
            cancel = true;
        } else if (!isSamIdValid(samId)) {
            mSamId.setError(getString(R.string.error_invalid_email));
            focusView = mSamId;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            ScanQrCode();
        }
    }

    private void ScanQrCode() {
        IntentIntegrator scanIntegrator = new IntentIntegrator(this);
        scanIntegrator.setPrompt("Scan");
        scanIntegrator.setBeepEnabled(true);

        //enable the following line if you want QR code
        scanIntegrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        scanIntegrator.setCaptureActivity(CaptureActivityAnyOrientation.class);
        scanIntegrator.setOrientationLocked(true);
        scanIntegrator.setBarcodeImageEnabled(true);
        scanIntegrator.initiateScan();
    }

    private boolean isSamIdValid(String samId) {
        //TODO: Replace this with your own logic
        return samId.length() > 0;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanningResult != null) {
            if (scanningResult.getContents() != null) {
                String scanContent = scanningResult.getContents(); //data
                try {
                    mStringData = KeyGenerate.decrypt(scanContent,mSamId.getText().toString());
                    if(!mStringData.equals("")) {
                        showProgress(true);
                        mPresenter.setConfig(new Gson().fromJson(mStringData, ConfigPOS.class));
                        mPresenter.retrieveToken();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            Toast.makeText(this, "Nothing scanned", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void updateToken(Token token) {
        if(token != null) {
            Log.d(Constants.Tag,"go here token");
            @SuppressLint("CommitPrefEdits") SharedPreferences.Editor data = getSharedPreferences(Constants.PREFEREENCES_NAME, MODE_PRIVATE).edit();
            data.putString(getString(R.string.KV_AT), token.getAccessToken());
            data.apply();
            mPresenter.retrieveBranch(token.getAccessToken());
        } else {
            showProgress(false);
        }
    }

    @Override
    public void updateBranch(BranchKiot branchKiot) {
        showProgress(false);
        if(branchKiot != null) {
            List<String> stringList = new ArrayList<>();
            for(int i = 0 ; i < branchKiot.getData().size() ; i ++) {
                stringList.add(branchKiot.getData().get(i).getBranchName());
            }

            SelectBranchsDialog selectBranchsDialog = SelectBranchsDialog.newInstance(stringList);
            selectBranchsDialog.show(getSupportFragmentManager(),SelectBranchsDialog.TAG);
        }
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    @SuppressLint("ObsoleteSdkInt")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void selectedItem(int i) {
        BranchKiot branchKiot = mPresenter.getDataBranch();
        if(branchKiot == null) {
            return;
        }
        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor token = getSharedPreferences(Constants.PREFEREENCES_NAME, MODE_PRIVATE).edit();
        token.putInt(getString(R.string.branchId), branchKiot.getData().get(i).getId());
        token.apply();
        if(mCheckbox.isChecked()) {
            PreferencesUtils.getInstance().saveStateInput(mSamId.getText().toString());
        } else {
            PreferencesUtils.getInstance().saveStateInput(null);
        }
        Bundle bundle = new Bundle();
        bundle.putString("StringData",mStringData);
        startActivity(new Intent(LoginActivity.this, MainActivity.class).putExtras(bundle));
        LoginActivity.this.finish();
    }
}
