package vn.cns.vinaidmerchant.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vn.cns.vinaidmerchant.Adapter.ListViewModel;
import vn.cns.vinaidmerchant.Adapter.RecyclerViewAdapter;
import vn.cns.vinaidmerchant.BaseActivity;
import vn.cns.vinaidmerchant.Constants;
import vn.cns.vinaidmerchant.CustomDialog;
import vn.cns.vinaidmerchant.Model.ConfigPOS;
import vn.cns.vinaidmerchant.Model.KiotVietModel;
import vn.cns.vinaidmerchant.Model.Products.Product;
import vn.cns.vinaidmerchant.Model.RequestAddItem;
import vn.cns.vinaidmerchant.Model.ResultSlotNumber.ResultSlotNumber;
import vn.cns.vinaidmerchant.Presenter.MainPresenter;
import vn.cns.vinaidmerchant.Presenter.MainPresenterImp;
import vn.cns.vinaidmerchant.R;
import vn.cns.vinaidmerchant.Service.API_Service;
import vn.cns.vinaidmerchant.Service.URLUtils;
import vn.cns.vinaidmerchant.Utils.FlowUtils;
import vn.cns.vinaidmerchant.View.MainView;

public class MainActivity extends BaseActivity implements MainView,
        AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener, FlowUtils.OnOKclickDialog,
        RecyclerViewAdapter.OnDataChangedListenerRV, CustomDialog.OnclickButtonOK, NavigationView.OnNavigationItemSelectedListener {

    private String KV_AT;
    private Product mProduct;
    private ResultSlotNumber mResultSlotNumber;
    private MainPresenter mPresenter;
    private ConfigPOS mConfigPOS;
    private ArrayAdapter<String> mSpinnerArrayAdapter;
    private RecyclerViewAdapter mAdapter;
    private List<ListViewModel> mListViewModel;
    private List<KiotVietModel> mKiotVietModel;
    private int total_quantity_mcp;
    private int total_quantity_kiot;
    private int spinner_selection_index = 0;
    private int temp_mcp, temp_kiot;
    private int branchId;
    private boolean isReloadData;

    @BindView(R.id.spinner_main)
    Spinner spinner_main;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.total_quantity_mcp)
    TextView quantity_mcp;
    @BindView(R.id.total_quantity_kiot)
    TextView quantity_kiot;
    @BindView(R.id.total_slot)
    TextView total_slot;
    @BindView(R.id.toolbar_main)
    Toolbar toolbar_main;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.design_navigation_view)
    NavigationView design_navigation_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FlowUtils.getInstance().setOnclickDialogListener(this);

        ButterKnife.bind(this);
        InitObject();
    }

    private void InitObject() {

        setSupportActionBar(toolbar_main);
        ActionBar actionBar = getSupportActionBar();
        Objects.requireNonNull(actionBar).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(MainActivity.this,drawerLayout,R.string.open,R.string.close);
        drawerToggle.syncState();
        toolbar_main.setNavigationOnClickListener(v -> drawerLayout.openDrawer(GravityCompat.START));
        design_navigation_view.setNavigationItemSelectedListener(this);

        mListViewModel= new ArrayList<>();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new RecyclerViewAdapter(mListViewModel);
        mAdapter.setListener(this);
        recyclerView.setAdapter(mAdapter);

        SharedPreferences prefs = getSharedPreferences(Constants.PREFEREENCES_NAME, MODE_PRIVATE);
        KV_AT = prefs.getString(getString(R.string.KV_AT),null);
        branchId = prefs.getInt(getString(R.string.branchId),0);

        mPresenter = new MainPresenterImp(this,branchId);
        String datafromLogin = getIntent().getStringExtra("StringData");
        if(datafromLogin != null) {
            Log.d(Constants.Tag,datafromLogin);
            mConfigPOS = new Gson().fromJson(datafromLogin,ConfigPOS.class);
        }

        Map<String,String> headerMap = new HashMap<>();
        headerMap.put("Retailer",mConfigPOS.getRetailer());
        headerMap.put("Authorization","Bearer " + KV_AT);
        mPresenter.retrieveProduct(headerMap,branchId);
    }

    int count = 0;

    @Override
    public void onBackPressed() {
        if(count == 0) {
            Toast.makeText(this,"Nhấn một lần nữa để thoát",Toast.LENGTH_SHORT).show();
            count++;
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    count = 0;
                }
            },3000);
        } else if (count == 1) {
            count = 0;
            super.onBackPressed();
        }
    }

    @OnClick(R.id.done)
    public void CheckAndSend() {
        mPresenter.processDatabeforeSend(mListViewModel,mKiotVietModel,mConfigPOS.getTerminalId());
    }

    @Override
    public void updateProduct(Product product,List<KiotVietModel> kiotVietModels,int total_quantity_kiot) {
        if(product != null && kiotVietModels != null) {
            mKiotVietModel = kiotVietModels;
            mProduct = product;
            this.total_quantity_kiot = total_quantity_kiot;
            temp_kiot = total_quantity_kiot;
            quantity_kiot.setText(String.valueOf(total_quantity_kiot));
            if(isReloadData) {
                mPresenter.processDataAfterTake(mResultSlotNumber,mProduct);
                dismissLoadingDialog();
                isReloadData = false;
            } else {
                URLUtils urlUtils = new URLUtils(mConfigPOS.getMCP_host());
                Log.d(Constants.Tag,"host: " + mConfigPOS.getMCP_token());
                API_Service api = urlUtils.getService();
                mPresenter.retrieveSlotNumber(api, mConfigPOS.getMCP_token(), mConfigPOS.getTerminalId());
            }
        }
    }

    @Override
    public void updateSlotNumber(ResultSlotNumber resultSlotNumber,int total_quantity_mcp) {
        if(resultSlotNumber == null) {
            return;
        }

        this.total_quantity_mcp = total_quantity_mcp;
        temp_mcp = total_quantity_mcp;
        quantity_mcp.setText(String.valueOf(total_quantity_mcp));

        showLog("resultSlotNumber success");
        if(resultSlotNumber.getContent().size() == 0) {
            ShowToast("Thiết bị chưa được thiết lập");
            showLog("resultSlotNumber = 0");
            return;
        }

        mResultSlotNumber = resultSlotNumber;
        mPresenter.processDataAfterTake(mResultSlotNumber,mProduct);
    }

    @Override
    public void updateListViewModel(List<ListViewModel> listViewModels) {
        showLog("size: " + listViewModels.get(0).getProductCode());
        mListViewModel.addAll(listViewModels);
        mAdapter.notifyDataSetChanged();
        total_slot.setText(String.valueOf(mListViewModel.size()));
    }

    @Override
    public void updateSpinnerValue(List<String> spinnerValue) {
        if(spinnerValue == null) {
            return;
        }
        mSpinnerArrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,spinnerValue);
        spinner_main.setAdapter(mSpinnerArrayAdapter);
        spinner_main.setSelection(0);
        spinner_main.setOnItemSelectedListener(this);
    }

    @Override
    public void updateDataShow(int mcp, int kiot, int number_slot) {
        if(number_slot == 0) {
            spinner_main.setSelection(0);
        } else {
            quantity_mcp.setText(String.valueOf(mcp));
            quantity_kiot.setText(String.valueOf(kiot));
            total_slot.setText(String.valueOf(number_slot));
            temp_mcp = mcp;
            temp_kiot = kiot;
        }
    }

    @Override
    public void updateIndexAfterFind(int position) {
        Log.d(Constants.Tag,position + 1 + " vị trí");
        if(position + 1 == spinner_selection_index) {
            if( position == -1) {
                mAdapter.getFilter().filter("Tất cả");
            } else {
                mAdapter.getFilter().filter(mProduct.getData().get(position).getCode());
            }
        } else {
            spinner_main.setSelection(position + 1);
        }
    }

    @Override
    public void updateProcessDataBeforeSend(String message, List<RequestAddItem> requestAddItems) {
        if(message != null) {
            ShowToast("Số lượng " + message + " vui lòng chỉnh lại");
        } else {
            URLUtils urlUtils = new URLUtils(mConfigPOS.getMCP_host());
            API_Service api = urlUtils.getService();
            mPresenter.retrieveRequestNewSlot(api,mConfigPOS.getMCP_token(),requestAddItems);
        }
    }

    @Override
    public void updateSendRequestNewSlot(int code) {
        switch (code) {
            case 200:
                ShowToast("Update Vend Success");
                break;
            default:
                ShowToast("Update Vend Fail " +  code);
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            case R.id.spinner_main:
                Log.d(Constants.Tag,"go here");
                ((TextView) adapterView.getChildAt(0)).setTextColor(Color.WHITE);
                ((TextView) adapterView.getChildAt(0)).setTextSize(16);
                if(i == 0) {
                    mAdapter.getFilter().filter(spinner_main.getSelectedItem().toString());
                } else {
                    Log.d(Constants.Tag + " vị trí filter",mProduct.getData().get(i-1).getCode());
                    mAdapter.getFilter().filter(mProduct.getData().get(i-1).getCode());
                }
                spinner_selection_index = i;
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onClickOk() {

    }

    @Override
    public void onDataChangedListener(ListViewModel listViewModel) {
        CustomDialog customDialog = CustomDialog.newInstance(listViewModel,mKiotVietModel);
        customDialog.show(getSupportFragmentManager(),CustomDialog.TAG);
    }

    @Override
    public void filterResult(List<ListViewModel> mListViewModelFiltered) {
        if(mListViewModel.size() > mListViewModelFiltered.size()) {
            mPresenter.CalculateDataFilter(mListViewModelFiltered, mKiotVietModel,false);
        } else if (mListViewModelFiltered.size() == 0) {
            spinner_main.setSelection(0);
        } else {
//            temp_mcp = total_quantity_mcp;
//            temp_kiot = total_quantity_kiot;
//            quantity_mcp.setText(String.valueOf(total_quantity_mcp));
//            quantity_kiot.setText(String.valueOf(total_quantity_kiot));
//            total_slot.setText(String.valueOf(mListViewModel.size()));
            mPresenter.CalculateDataFilter(mListViewModelFiltered, mKiotVietModel,true);
        }
    }

    @Override
    public void SetChangeInventory(int i, int quantity) {
        int delta = quantity - mListViewModel.get(i).getQuantity();
        temp_mcp += delta;
        total_quantity_mcp += delta;
        quantity_mcp.setText(String.valueOf(temp_mcp));
        mListViewModel.get(i).setQuantity(quantity);
    }

    @Override
    public void clickOK(ListViewModel listViewModel, int oldQuantity, boolean isTheSame, String name, int newQuantity) {
        Log.d(Constants.Tag + " chọn",name);
        mPresenter.FindIndex(name,mKiotVietModel);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.settings:
//                startActivity(new Intent(MainActivity.this,SettingsActivity.class));
                break;
            case R.id.reload:
                total_quantity_mcp = 0;
                isReloadData = true;
                Map<String,String> headerMap = new HashMap<>();
                headerMap.put("Retailer",mConfigPOS.getRetailer());
                headerMap.put("Authorization","Bearer " + KV_AT);
                mPresenter.retrieveProduct(headerMap,branchId);
                break;
            case R.id.logout:
                startActivity(new Intent(MainActivity.this,LoginActivity.class));
                this.finish();
                break;
        }
        return false;
    }

    
    private void anotherFunction() {
        Log.d(Constants.Tag, "hello world");
    }
    
    private void testFunctionBitbucket() {
        Log.d(Constants.Tag,"test");
    }

    private void testFunctionAgain() {};
}
