package vn.cns.vinaidmerchant.Service;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import vn.cns.vinaidmerchant.Model.Branchs.BranchKiot;
import vn.cns.vinaidmerchant.Model.Products.Product;
import vn.cns.vinaidmerchant.Model.RequestAddItem;
import vn.cns.vinaidmerchant.Model.ResultSlotNumber.ResultSlotNumber;
import vn.cns.vinaidmerchant.Model.Token;

/**
 * Created by Ho Dong Trieu on 01/14/2019
 */
public interface API_Service {

    /**
     * get Token KiotViet
     * @param fieldMap: client_id, client_secret, grant_type = client_credentials, scopes = PublicApi.Access
     * @return token.
     */
    @Headers("Content-Type:application/x-www-form-urlencoded")
    @POST("connect/token")
    @FormUrlEncoded
    Call<Token> RETURN_ACCESSTOKEN_KIOTVIET(@FieldMap Map<String, String> fieldMap);

    /**
     * Get all product from KiotViet
     * @param headers Header map: token + retailer
     * @param branchIds branchIds
     * @return Product.
     */
    @Headers("Content-Type: application/json")
    @GET("products?includeInventory=true&pageSize=100&orderBy=basePrice&orderDirection=DESC")
    Call<Product> RETURN_PRODUCT_KIOT(@HeaderMap Map<String, String> headers, @Query("branchId") int branchIds);

    /**
     * Get all branch and select.
     * @param headers token + retailer
     * @return branches
     */
    @Headers("Content-Type: application/json")
    @GET("branches")
    Call<BranchKiot> RETURN_BRANCH_KIOTVIET(@HeaderMap Map<String,String> headers);

    @Headers("Content-Type:application/json")
    @POST("api/kiotInfoes")
    Call<Void> POST_ADDNEWSLOT(@Header("Authorization") String MCP_accessToken, @Body List<RequestAddItem> requestAddItemList);

    @Headers("Content-Type:application/json")
    @GET("api/kiotInfoes/findByTerminalId")
    Call<ResultSlotNumber> GET_SLOT_NUMBERS(@Header("Authorization") String MCP_accessToken, @Query("terminalId") String terminalId , @Query("size") int size, @Query("page") int pageNumber);
}
