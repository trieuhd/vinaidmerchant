package vn.cns.vinaidmerchant.Service;

/**
 * Created by Ho Dong Trieu on 01/14/2019
 */
public class URLUtils {

    private static String Base_Url;
    public URLUtils(String base_Url) {
        Base_Url = base_Url;
    }

    public API_Service getService() {
        return RetrofitService.getClient(Base_Url).create(API_Service.class);
    }
}
